import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
//import  MainView from './src/components/screens/MainView';
import  { AppDrawer } from './src/components/config/routes';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar hidden={true} />       
        <AppDrawer/>
      </View>   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
