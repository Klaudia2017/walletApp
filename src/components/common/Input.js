
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';

const Input = ({onChangeText, value, placeholder}) => {
    return (
        <View style={styles.inputContainer}>
            <TextInput
            multiline
            style={styles.input}
            placeholder={placeholder}
            value={value}
            autoFocus={true}
            underlineColorAndroid='transparent'
            onChangeText={(text) => onChangeText(text)}
            />
        </View>
          
    );
};

const styles = StyleSheet.create({
    inputContainer: {
     
     borderRadius: 20,
     borderWidth: 0.5, 
     borderColor: 'gray',
     backgroundColor: 'white'
    },
    input: {
        fontSize: 16,
        paddingLeft: 15
    },
});

export  {Input};
