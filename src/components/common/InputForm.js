import React from 'react';
import { TextInput, View, Text } from 'react-native';
import {APP_COLORS} from '../../style/color';

const InputForm = ({ style, active, onFocus, editable, label, value, onChangeText, placeholder, secureTextEntry, onBlur, symboleMonnaie, keyboardType }) => {
  const { symboleContainer, inputStyle, labelStyle, containerStyle, symboleStyle } = styles;

        
        return (
          <View 
          style={ (active) ? [containerStyle, {...style}] : [containerStyle , {...style}, {borderWidth: 0.5}]} 
          >
            <Text 
            style={ (active) ? labelStyle : [labelStyle, ,{color: APP_COLORS.secondaryText || 'gray'}]} 
            >{label}</Text>
            <View style={symboleContainer} >
              <TextInput
                keyboardType={keyboardType || 'default'}
                onFocus={onFocus}
                editable={editable}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                autoCorrect={false}
                style={inputStyle}
                value={value}
                onChangeText={onChangeText}
                onBlur= {onBlur}
                autoCapitalize = 'none'
                underlineColorAndroid="rgba(0,0,0,0)"   
                autoCorrect={false}
                maxLength={10}
              /> 
              <View style={symboleStyle} >
                <Text style={{fontSize: 16}}>{symboleMonnaie}</Text>
              </View> 
            </View>
          </View>)
};

const styles = {
  containerStyle: { 
    flex: 1,       
    flexDirection: 'row',
    alignItems: 'center',     
    paddingHorizontal: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: APP_COLORS.secondaryText  || 'gray',  
    width: '100%',
    height: 38,
    backgroundColor: APP_COLORS.primary || 'white',
  },
  inputStyle: {
    color:  APP_COLORS.primaryText || 'black',
    fontSize: 18,
    lineHeight: 18,
    flex: 8,     
    paddingLeft: 15, 
    alignContent : 'flex-start',
  },
  labelStyle: {   
    fontSize: 16,
    flex: 8,
  },
  symboleStyle : {
    flex: 2,    
    alignItems: 'center',
  },
  symboleContainer: {
    flex: 6,
    flexDirection: 'row',
    alignItems: 'center',
  },
};

export { InputForm };