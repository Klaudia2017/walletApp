import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {APP_COLORS} from '../../style/color';

class MyListItem extends React.PureComponent {
    _onPress = () => {
      this.props.onPressItem(this.props.id);
    };
  
    render() {
      const textColor = this.props.selected ? "red" :  "gray" ;
      const { buttonStyle, textStyle } = styles;
      return (
        <TouchableOpacity 
        onPress={this._onPress}
        style={ [buttonStyle, {borderColor: "gray" }] }
        >
          <View>
            <Text style={ [textStyle, {color: textColor}] } >
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
}

const styles = {
    textStyle: {
      alignSelf: 'center',
      fontSize: 16 ,    
      fontWeight: '600',
      paddingTop: 10,
      paddingBottom: 10,
    },
    buttonStyle: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      alignSelf: 'stretch',
      borderRadius: 5,
      borderWidth: 1,
      backgroundColor: APP_COLORS.primary || 'white',
    },
  };

export {MyListItem};