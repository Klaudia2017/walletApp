import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {APP_COLORS} from '../../style/color';

const ActionButton = ({style, onPress, children}) => {
    return (
        <View style={styles.bcgButton}>
            <TouchableOpacity style={[styles.circleButton, style]} onPress={onPress}>
                {children}
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    circleButton: {
        height: 60,
        width: 60,
        borderRadius: 30,   
        backgroundColor: APP_COLORS.accent || 'gray',  
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor:  '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        zIndex: 1,
    }, 
    bcgButton: {
        height: 60,
        width: 60,
        borderRadius: 30, 
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        zIndex: 0,
    }
});

export {ActionButton};
