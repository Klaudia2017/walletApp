import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';


const CardSection = ({children, last, style}) => {
  return (
    <View style={ (last) ? [styles.containerStyle, {...style}, {paddingBottom: 10}] : [styles.containerStyle, {...style}] }>
      {children} 
    </View>
  );
};

const styles = {
  containerStyle: {
    paddingHorizontal: 10,
    paddingTop: 10,
    flexDirection: 'row',
    borderColor: APP_COLORS.lines || 'gray',
    position: 'relative',
  }
};

export {CardSection};


