import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {APP_COLORS} from '../../style/color';

class CategoryItem extends React.PureComponent {
    _onPress = () => {
      this.props.onPressItem(this.props.id);
    };
  
    render() {
    
      const { buttonStyle, textStyle } = styles;
      return (
        <TouchableOpacity 
        onPress={this._onPress}
        style={ [buttonStyle, {borderColor:  APP_COLORS.primaryText }] }
        >   
          <View>
            <Text style={ [textStyle, {color:   APP_COLORS.primaryText}] } >
              {this.props.title}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  }

  const styles = {
    textStyle: {
      alignSelf: 'center',
      fontSize: 16 ,    
      fontWeight: '600',
      paddingTop: 10,
      paddingBottom: 10,
    
    },
    buttonStyle: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      alignSelf: 'stretch',
      borderRadius: 5,
      borderWidth: 1,
      backgroundColor: APP_COLORS.input || 'white',
    },
  
    
  };

  export {CategoryItem};