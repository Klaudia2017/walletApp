import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color';

const ButtonColor = ({ onPress, children, color }) => {
  const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;

  return (
    <TouchableOpacity onPress={onPress} 
    style={ (color) ? [buttonStyle, {backgroundColor: color} ] : [buttonStyle, {backgroundColor: APP_COLORS.primaryAction} ]}>
      <Text style={textStyle} >
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 16 ,    
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10,
    color: APP_COLORS.primary || 'white',
  },
  buttonStyle: {
    flex: 1,
    alignSelf: 'stretch',
    borderRadius: 5,
    borderWidth: 1,
  
  },
  buttonDisabled: {
    backgroundColor: APP_COLORS.primary || 'white',
    borderColor: APP_COLORS.secondaryText || 'gray',
  },
  buttonEnabled: {
    backgroundColor: APP_COLORS.primaryAction || 'red',
    borderColor: APP_COLORS.primary || 'white',
  }
};

export {ButtonColor};


