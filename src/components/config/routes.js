import Icon from 'react-native-vector-icons/Ionicons';
import { TabNavigator, StackNavigator, DrawerNavigator} from 'react-navigation';

import NewExpenseComponent from '../screens/NewExpenseComponent';
import ResumeAnnualComponent from '../screens/ResumeAnnualComponent';
import AddBudgetComponent from '../screens/AddBudgetComponent';
import ListOfDepenses from '../screens/ListOfDepenses';
import AddCategoryComponent from '../screens/AddCategoryComponent';
import CategoryList from '../screens/CategoryList';
import SettingsComponent from '../screens/SettingsComponent';


//StackNavigator

export const StackNavigatorAddBudget = StackNavigator({
    AddBudgetView: {
      screen: AddBudgetComponent,
    },
});

export const StackNavigatorNewExpense = StackNavigator({
    NewExpenseView: {
      screen: NewExpenseComponent,
    },
});

export const StackNavigatorAddCategory = StackNavigator({
    AddCategoryView: {
          screen: AddCategoryComponent,
    },
});

export const StackNavigatorListOfDepenses = StackNavigator({
    ListOfDepensesView: {
          screen: ListOfDepenses,
    },
});

export const StackNavigatorResumeAnnual = StackNavigator({
    ResumeAnnualView: {
      screen: ResumeAnnualComponent,
    },
});

export const StackNavigatorCategoryList = StackNavigator({
    CategoryListView: {
        screen: CategoryList,
    },
});
    
export const StackNavigatorSettingsComponent = StackNavigator({
    SettingsView: {
        screen: SettingsComponent,
    },
});



//Drawer Navigator
//https://reactnavigation.org/docs/navigators/drawer#DrawerNavigatorConfig

export const AppDrawer = DrawerNavigator({
    NewExpenseScreen: {
        screen: StackNavigatorNewExpense,
        navigationOptions : {
             drawer: {
                label: 'NEW EXPENSE',
             },
         },
    },
    AddBudgetScreen: {
        screen: StackNavigatorAddBudget,
        navigationOptions : {
             drawer: {
                label: 'AJOUTER BUDGET',
             },
         },
    },
    AddCategoryScreen: {
        screen: StackNavigatorAddCategory,
        navigationOptions : {
             drawer: {
                label: 'UNE NOUVELLE CATÉGORIE',
             },
         },
    },
    CategoryListScreen: {
        screen: StackNavigatorCategoryList,
        navigationOptions : {
             drawer: {
                label: 'LIST DES CATÉGORIES',
             },
         },
    },
    ListOfDepensesScreen: {
        screen: StackNavigatorListOfDepenses,
        navigationOptions : {
             drawer: {
                label: 'LIST DES DEPENSES',
             },
         },
    },
    SettingsScreen: {
        screen: StackNavigatorSettingsComponent,
        navigationOptions : {
            drawer: {
                label: 'PARAMETRES',
            },
        },
    },       
    ResumeAnnualScreen: {
        screen: StackNavigatorResumeAnnual,
        navigationOptions : {
            drawer: {
                label: 'RESUME',
            },
        },
    },    
       
});
   

     /*


      AddCategoryScreen: {
           screen: StackNavigatorAddCategory,
           title : 'UNE NOUVELLE CATÉGORIE'
       },
       ViewCategoryList: {
           screen: StackNavigatorCategoryList,
           title : 'LIST DES CATÉGORIES'
       },
       ViewListOfDepenses: {
           screen: StackNavigatorListOfDepenses,
           title : 'ListOfDepenses'
       },
       BudgetEditView: {
           screen: StackNavigatorBudgetEdit,
           title : 'EDITER LE BUGET'
       },
       ViewBudgetEdit: {
           screen: StackNavigatorVisualisation,
           title : 'VISUALISATION'
       },
       ViewNewExpense: {
           screen: StackNavigatorNewExpense,
           title : 'AJOUTEZ'
       }

       */