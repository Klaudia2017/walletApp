import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, TouchableHighlight, AsyncStorage, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { StackNavigator } from 'react-navigation';
import { Container, Header, Title, Content, Button, Icon as IconeNB, Right, Body, Left, Picker, Form,  H3, Item as FormItem } from "native-base";
const Item = Picker.Item;
import {Icon as Ionicons} from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/EvilIcons';
import {APP_COLORS} from '../../style/color';
import { InputForm, ButtonForm, CardSection, Card, FadingView, ErrorModal } from '../common';

class NewExpenseComponent extends Component {

    constructor (props) {
        super(props)
        this.state = {  

            inputValueLabel: "Montant depensé",
            inputValueText: '',
            inputValuePlaceholder: '0',

            inputDescLabel: "Description",
            inputDescText: '',      
            inputDescPlaceholder: 'lait',
            
            inputCategoryLabel: "Catégorie",
            choosenCategory: null,

            errorVisible: false,
            error: '',

            categoryList : [],

            listOfDepenses: [],
            indexItemtoModify : -1,
            oldDepense: {},
        }
    }  
    static navigationOptions =({navigation}) => {
        return {
            title: 'UNE NOUVELLE DÉPENSE',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
                paddingHorizontal: 15,
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                alignSelf: 'center', 
                paddingRight: 50, 
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }

 componentDidMount (){
        let {params} = this.props.navigation.state;
        if(params){
            if(params.depenseID) this.setState({'indexItemtoModify': params.depenseID});
            this.findDepense({params})
        }
        this.getListCategories().done();
    }
    
    // obtenir la liste des catégories
    getListCategories = async () => {
        try {
            categoryList = await AsyncStorage.getItem('categoryList');
            if(categoryList){
                categoryList = JSON.parse(categoryList);
                this.setState({categoryList : categoryList});
            } else {
                error = 'Liste de categories vidé' ;
                this.toggleError({error});
            }
            
        } catch (error) {
            error = 'Erreur recuperation de catégories' ;
            this.toggleError({error});
        }
    }

    findDepense= async ({params}) => {
        try {
            listOfDepenses = await AsyncStorage.getItem('listOfDepenses');
            if(listOfDepenses){
                listOfDepenses = JSON.parse(listOfDepenses); 
                let indexItemtoModify = this.state.indexItemtoModify;
                let OldExpense = {};
                listOfDepenses = listOfDepenses.map((el, index) => {
                    if(el.time == indexItemtoModify){
                        OldExpense = el;
                    }
                    return el;
                },OldExpense);
                this.setState({ listOfDepenses, oldDepense, inputValueText: String(OldExpense.value), inputDescText: String(OldExpense.description), choosenCategory: OldExpense.category })
            } 
        } catch (error) {
            error = 'Erreur, n\'a pas pu obtenir d\'informations sur votre ancien dépôt essayez encore une fois' ;
            this.toggleError({error});
        }  
    }
    ModifyDepense = async () => {
        let totalOfExpenses;
        let { indexItemtoModify, listOfDepenses, oldDepense, inputValueText, inputDescText, choosenCategory} = this.state;
        let depense = {
            time: indexItemtoModify,
            value: inputValueText,
            description: inputDescText,
            category: choosenCategory,
        };
        
        if( depense.value.length > 0 && depense.description.length > 1  && depense.category != null ) {
            try {
                totalOfExpenses = await AsyncStorage.getItem('totalOfExpenses');
                if(listOfDepenses && totalOfExpenses){
                    listOfDepenses = listOfDepenses.map((el) => {
                        if(el.time == indexItemtoModify){
                            el = depense;
                        }
                        return el;
                    });
                    totalOfExpenses = JSON.parse(totalOfExpenses);
                    totalOfExpenses = Number(totalOfExpenses) + Number(depense.value) - Number(oldDepense.value);
                } 
                await AsyncStorage.multiSet([ ['listOfDepenses', JSON.stringify(listOfDepenses)], ['totalOfExpenses', JSON.stringify(totalOfExpenses)] ]);
                this.returnToListOfDepenses();
            } catch (error) {
                error = 'Erreur au niveaun d\'ajout de dépense, essayez encore une fois' ;
                this.toggleError({error});
            }
        } else{
            error = 'Veuillez entrer toutes les informations demandées' ;
            this.toggleError({error});
        }
    }
    
    //afficher les options du sélecteur
    PickerRender = () => {
        let {categoryList} = this.state;
        return categoryList.map( (el, i) => {
            return (  <Item key={i} label={el.name} value={el} />);
        });
    }
    //changer de catégorie
    onCategoryChoice = (value) => {
        this.setState({
            choosenCategory: value,
        });
    }
    //aller à l'écran d'ajout de catégorie
    goToAddCategory = () => {
        var {navigate} = this.props.navigation;
        navigate('AddCategoryView');
    }
    //montrer le sélecteur dans le nouvel écran
    showCategories = () => { 

       return  (<Picker
        renderHeader={backAction =>
            <Header style={{ backgroundColor: APP_COLORS.darkPrimary }}>
              <Left>
                <Button transparent onPress={backAction}>
                  <IconeNB name="arrow-back" style={{ color: APP_COLORS.primary }} />
                </Button>
              </Left>
              <Body style={{ flex: 3 }}>
                <Title style={{ color: APP_COLORS.primary }}>CHOISISSEZ</Title>
              </Body>
              <Right> 
                <Icon name="pencil" size={35} style={{color: APP_COLORS.primary }}  onPress={ () => this.goToAddCategory()}/>
              </Right>
            </Header>}
        mode="dropdown"  
        placeholder="..."
        iosHeader="CHOISIR"
        selectedValue={this.state.choosenCategory}
        onValueChange={ (value) => this.onCategoryChoice(value)}
      >
       {this.PickerRender()}
      </Picker>)
    }

    //exécuter après avoir cliqué sur le bouton accepter
    onAcceptPress = () => {
        let {indexItemtoModify} = this.state;
        if(indexItemtoModify != -1){
            this.ModifyDepense().then(() => {}).done();
        }else{
            this.saveDepense().done();
        }   
    }
    //sauver nouveau depense aux mémoires de téléphone
    saveDepense = async () => { 
        
        let listOfDepenses;
        let totalOfExpenses;

        let depense = {
            time: new Date(),
            value: this.state.inputValueText,
            description: this.state.inputDescText,
            category: this.state.choosenCategory,
        };
        
        if( depense.value.length > 0 && depense.description.length > 1  && depense.category != null ) {
            try {
                listOfDepenses = await AsyncStorage.getItem('listOfDepenses');
                totalOfExpenses = await AsyncStorage.getItem('totalOfExpenses');
                if(listOfDepenses && totalOfExpenses){
                    listOfDepenses = JSON.parse(listOfDepenses);   
                    listOfDepenses.push(depense);
                    totalOfExpenses = JSON.parse(totalOfExpenses);
                    totalOfExpenses = Number(totalOfExpenses) + Number(depense.value);
                } else {                   
                    listOfDepenses = [depense];  
                    totalOfExpenses = depense.value;
                }               
                await AsyncStorage.multiSet([ ['listOfDepenses', JSON.stringify(listOfDepenses)], ['totalOfExpenses', JSON.stringify(totalOfExpenses)] ]);
                this.returnToListOfDepenses();
            } catch (error) {
                error = 'Erreur au niveaun d\'ajout de dépense, essayez encore une fois' ;
                this.toggleError({error});
            }
        } else{     
            error = 'Veuillez entrer toutes les informations demandées' ;
            this.toggleError({error});
        }
    }

    //naviguer vers l'écran 'ListOfDepenses'
    returnToListOfDepenses = () => {
        let {navigation} = this.props;
        navigation.navigate('ListOfDepensesView');
    }

    // Afficher le message d'erreur et disparaître après 3 secondes
    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    // supprimer tous les caractères non numériques du texte d'entrée
    onChangedNumber = ({inputValueText}) => {
        this.setState({
            inputValueText: inputValueText.replace(/[^0-9]/g, ''),
        });
    }
    
 render() {
        
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.container}>
                    <Card >
                        <CardSection> 
                            <InputForm 
                            active={true}
                            editable={true}  
                            label={this.state.inputValueLabel}
                            value={this.state.inputValueText}
                            onChangeText={(inputValueText) =>  this.onChangedNumber({inputValueText})}
                            placeholder={this.state.inputValuePlaceholder}
                            symboleMonnaie='€'
                            keyboardType={'numeric'}
                            onFocus={() => {}}
                            onBlur={() => {}}
                            /> 
                        </CardSection>
                        <CardSection> 
                            <InputForm 
                            active={true}
                            editable={true}
                            label={this.state.inputDescLabel}
                            value={this.state.inputDescText}
                            onChangeText={(inputDescText) => {this.setState({inputDescText})}}
                            placeholder={this.state.inputDescPlaceholder}
                            onFocus={() => {}}
                            onBlur={() => {}}
                            /> 
                        </CardSection>
                        <CardSection> 
                            <View style={styles.contentPickerStyle}>
                                <Text style={styles.labelPickerStyle}> Categorie </Text> 
                                <View style={styles.PickerStyle}> 
                                    {this.showCategories()}
                                </View>
                            </View>                         
                        </CardSection>
                        <CardSection last={true} >   
                            <ButtonForm  
                            onPress={ () => this.returnToListOfDepenses() }
                            >
                            ANNULER
                            </ButtonForm>                           
                            <View style={{width: 10}}></View>                        
                            <ButtonForm   
                            onPress={ () => this.onAcceptPress() }  
                            >
                            VALIDER
                            </ButtonForm>     
                        </CardSection>
                    </Card>
                    <ErrorModal visible={this.state.errorVisible}>{this.state.error}</ErrorModal>   
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    input: {  
        flex: 1,
        paddingHorizontal: 20,
    },
    button: {
        flex: 1,
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 35,
        height: 35,
        color: APP_COLORS.primary,
    },
    buttonText: {
        fontSize: 15,
        lineHeight: 15,
        color: APP_COLORS.primary,
    },
    contentPickerStyle: { 
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center', 
        paddingHorizontal: 8,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: APP_COLORS.secondaryText  || 'gray',  
        height: 40,
        width: '100%',
        backgroundColor: APP_COLORS.primary || 'white',
    },
    labelPickerStyle: {   
        fontSize: 16,
        flex: 8, 
    },
    PickerStyle: { 
        flex: 6,  
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 15, 
        alignContent : 'flex-start',
    },
    errorContainer: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 300, 
        marginTop: 40,
        alignSelf: 'center',  
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
    },
    errorText: {
        fontSize: 14,
        color: APP_COLORS.primary || 'white',
        lineHeight: 22,
    },    
});

export default NewExpenseComponent;


