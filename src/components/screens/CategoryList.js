
import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, FlatList, TouchableOpacity, AppRegistry } from 'react-native';
import { StackNavigator } from 'react-navigation';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import { LightButtonForm, Card, CardSection, MyListItem, ButtonForm, FadingView, ErrorModal } from '../common';
import Modal from 'react-native-modal';

class CategoryList extends Component {     

    constructor (props) {
        super(props)
        this.state = {
            error: '',
            errorVisible: false,

            data : [],
           
            isModalVisible: false,
            selectedItem: {},
        }   
    }
    static navigationOptions =({navigation}) => {
        return {
            title: 'LISTE DES CATÉGORIES',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
                paddingHorizontal: 15,
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                alignSelf: 'center', 
                paddingRight: 50, 
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }  
    
    componentDidMount() {
        this.getListCategories().done();
    }
    getListCategories = async () => {
        try {
            categoryList = await AsyncStorage.getItem('categoryList');
            categoryList = JSON.parse(categoryList);
            if(categoryList){
                this.setState({data : Array.from(categoryList)});
            } else{
                error = 'Liste de categories vide' ;
                this.toggleError({error});
            }
        } catch (error) {
            error = 'Erreur recuperation de catégories ' ;
            this.toggleError({error});
        }
    }
    removeCategory = async () => {
        let id = this.state.selectedItem.id;
        let categoryList = this.state.data;

        categoryList = categoryList.filter(item => item.id !== id)
        this.setState({ data : Array.from(categoryList) });
        try {
            await AsyncStorage.setItem('categoryList', JSON.stringify(categoryList));
        } catch (error) {
            error = 'Erreur au niveau des mis a jour de list des catégories  ' ;
            this.toggleError({error});
        }    
    }
  
    goToAddCategory = () => {
        var {navigate} = this.props.navigation;
        navigate('AddCategoryView');
    }

    toggleModal = () => {
       this.setState({ isModalVisible: !this.state.isModalVisible })
    }

    onPressItem = (item) => {
        this.toggleModal();
        this.setState({ selectedItem:  item });
    };

    goToModifyCategory = () => {
        let { selectedItem } = this.state;
        var {navigate} = this.props.navigation;
        navigate('AddCategoryView', {'categoryID': selectedItem.id });  
    }

    onRemoveCategoryClick = () => {
        this.removeCategory();
        this.toggleModal();
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }
    render() {
        const { navigate } = this.props.navigation;
        const { selected } = this.state;

        return (
            <View style={styles.container}>
                    <Card >  
                        <FlatList
                        data={this.state.data}
                        extraData={this.state}
                        keyExtractor={item => item.id}
                        renderItem={ ({item}) => 
                            <CardSection>
                                <MyListItem
                                id={item}
                                onPressItem={this.onPressItem}
                                title={item.name}
                                color={item.color}
                                />    
                            </CardSection>
                        }
                        />
                        
                        <CardSection last={true}>
                            <LightButtonForm
                            color={APP_COLORS.primaryAction}
                            onPress={() => {this.goToAddCategory()}}
                            >
                            ajouter une catégorie ...
                            </LightButtonForm>
                        </CardSection>
                    </Card> 
                    <ErrorModal visible={this.state.errorVisible}>{this.state.error}</ErrorModal>
                    <Modal isVisible={this.state.isModalVisible}>
                        <Card>
                            <TouchableOpacity onPress={this.toggleModal} style={{ alignItems: 'flex-end', paddingTop: 10, paddingRight: 10 }} >
                                <Icon name="close" size={25} style={{color: APP_COLORS.secondaryText}} />
                            </TouchableOpacity>                          
                            <CardSection>
                                <Text style={styles.modalText}>Que voudriez-vous faire ...</Text>
                            </CardSection>
                            <CardSection last={true}>
                                <ButtonForm onPress={ this.goToModifyCategory }>modifier</ButtonForm>
                                <View style={{width: 10}}></View>
                                <ButtonForm onPress={this.onRemoveCategoryClick}>supprimer</ButtonForm>
                            </CardSection>  
                        </Card>
                    </Modal>                               
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    button: {
        flex: 1,
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 40,
        height: 40,
        color: APP_COLORS.primary,
      },
    showlist : {
        paddingHorizontal: 10,
        paddingTop: 10,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: APP_COLORS.lines || 'gray',
        position: 'relative',
    },
    modalText: {
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 20,
        color: APP_COLORS.secondaryText,
    },
    errorContainer: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 300, 
        marginTop: 40,
        alignSelf: 'center',  
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
    },
    errorText: {
        fontSize: 14,
        color: APP_COLORS.primary || 'white',
        lineHeight: 22,
    },  
    
});

export default CategoryList;


