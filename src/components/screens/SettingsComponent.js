import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, TouchableHighlight, AsyncStorage } from 'react-native';
import { Card, CardSection, LightButtonForm } from '../common';
import Icon from 'react-native-vector-icons/EvilIcons';
import {APP_COLORS} from '../../style/color';

class SettingsComponent extends Component {

  static navigationOptions =({navigation}) => {
    return {
        title: 'PARAMÈTRES',
        headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
        headerStyle: { 
          backgroundColor: APP_COLORS.darkPrimary,  
          paddingHorizontal: 15,
        },
        headerTitleStyle: {
          color : APP_COLORS.primary,  
          alignSelf: 'center', 
          paddingRight: 50, 
          fontWeight: 'normal',
          fontSize: 15,
        }
    }  
  }
  clearStorage = async () => {
    try {
       await AsyncStorage.clear();
    } catch (error) {
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <Card>
          <CardSection last={true}>
            <LightButtonForm 
            color={APP_COLORS.primaryAction}
            onPress={() => this.clearStorage()}
            >
            effacer tous les résultats
            </LightButtonForm>
          </CardSection>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: APP_COLORS.lightPrimaryColor,
  },
});


export default SettingsComponent;
