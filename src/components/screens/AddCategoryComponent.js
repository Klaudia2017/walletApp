import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, ScrollView, TouchableOpacity, Keyboard, TouchableWithoutFeedback } from 'react-native';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import { InputSansLabel, ButtonForm, Card, CardSection, FadingView, ErrorModal } from '../common';
import { ColorPicker, TriangleColorPicker } from 'react-native-color-picker';

class AddCategoryComponent extends Component {
    constructor (props) {
        super(props)
        this.state = {

            inputLabel: '',
            inputText: '',
            inputPlaceholder: "Nom de catégorie",

            color: '',

            buttonTitle: "VALIDER",

            error : '',
            errorVisible: false,

            alert: 'pour valider votre choix de couleur, veuillez cliquer sur la barre ci-dessus',   
            alertVisible: false,
            
            indexItemtoModify: -1,
            categoryToModify: {},
            categoryList: [],
        }    
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'UNE NOUVELLE CATÉGORIE',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,
                paddingHorizontal: 15,
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                alignSelf: 'center', 
                paddingRight: 50, 
                fontWeight: 'normal',
                fontSize: 15,
            }
      }  
    }  

    componentDidMount (){  
    
        let {params} = this.props.navigation.state;
        if(params){
            if(params.categoryID) this.indexItemtoModify = params.categoryID ;
            this.findCategory()
        }              
    }
//if(params.categoryID) ; this.setState({'indexItemtoModify': params.categoryID})
    findCategory = async () => { 
        try {
            let categoryList = await AsyncStorage.getItem('categoryList');
            if(categoryList){
                // retirer la category de la list des categories à travers l'index
                categoryList = JSON.parse(categoryList);
                let {indexItemtoModify} = this.state;
                let categoryToModify = {};
                
                categoryList = categoryList.map((el, index) => {
                    if(el.id == indexItemtoModify){
                        categoryToModify = el;
                    }
                    return el;
                },categoryToModify);
                this.setState({ inputText: String(categoryToModify.name), color: String(categoryToModify.color), categoryList , oldDepense : categoryToModify })
            } 
        } catch (error) {
            error = 'Erreur, n\'a pas pu obtenir d\'informations sur votre ancien dépôt essayez encore une fois' ;
            this.toggleError({error});
        }       
    }

    modifyCategory = async () => {
    
        let { indexItemtoModify, categoryList, categoryToModify } = this.state;
        let newCategory = {
            id: indexItemtoModify,
            color:  this.state.color,
            name:   this.state.inputText,
        };

        if( newCategory.name.trim().length > 1 && newCategory.color.length > 1 && categoryList ) {
            try {
                //remplace le vieux category dans la liste
                categoryList = categoryList.map((el) => {
                    if(el.id == indexItemtoModify){
                        el = newCategory;
                    }
                    return el;
            });
            await AsyncStorage.setItem( 'categoryList', JSON.stringify(categoryList) );
            this.returnToListOfCategories();
            } catch (error) {
                error = 'Erreur au niveaun d\'ajout de dépense, essayez encore une fois' ;
                this.toggleError({error});
            }
        } else{
            error = 'Veuillez entrer toutes les informations demandées' ;
            this.toggleError({error});
        }
    }

    addCategory = async () => {

        let categoryList;
        let newCategory = {
            id:     new Date() ,
            color:  this.state.color,
            name:   this.state.inputText,
        }
       
        if( newCategory.name.trim().length > 1 && newCategory.color.length > 1 ){
            try {
                categoryList = await AsyncStorage.getItem('categoryList');
                if(categoryList){
                    categoryList = JSON.parse(categoryList);
                    categoryList.push(newCategory);  
                } else {
                    categoryList = [newCategory];
                }
                await AsyncStorage.setItem('categoryList', JSON.stringify(categoryList));
                this.returnToListOfCategories();
            } catch (error) {
                error = 'Erreur au niveau d\'ajout de catégorie ' ;
                this.toggleError({error});
            }
        } else if ( newCategory.name.trim().length < 1 && newCategory.color.length < 1 ) {
            error = 'Veuillez entrer toutes les informations demandées ' ;
            this.toggleError({error});
        } else if ( newCategory.name.trim().length < 1 && newCategory.color.length > 1 ) {
            error = 'Nom de catégorie incorrect ou trop court ' ;
            this.toggleError({error});  
        } else {
            this.toggleAlert();   
        }  
    }

    onAcceptButtonPress = () => {
        Keyboard.dismiss();  
        let {indexItemtoModify} = this.state;
        
        if(indexItemtoModify != -1){
            this.modifyCategory().done();
        }else{
            this.addCategory().done();
        }   
    }

    returnToListOfCategories = () => {
        let {navigation} = this.props;
        navigation.navigate('CategoryListView');
    } 

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    toggleAlert = () => {

        let alert = 'pour valider votre choix de couleur, veuillez cliquer sur la barre ci-dessus'; //this.state.notification ;
        this.setState({ alertVisible: !this.state.alertVisible });
        setTimeout(() => {this.setState({alertVisible: !this.state.alertVisible })}, 4000);
    }

    renderAlert = () => {      
        const { alertContainer, errorText } = styles;
        const { alertVisible } = this.state;
        
            return ( <FadingView visible={alertVisible} style={alertContainer}>  
                        <Text style={errorText} >
                            {this.state.alert}    
                        </Text>
                    </FadingView>  
            );     
    }
    render() {

        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
                <View style={styles.container}>
                
                    <View style={{flex: 1}}>
                        <ErrorModal visible={this.state.errorVisible}>{this.state.error}</ErrorModal>
                    </View>
                    <Card style={{flex: 7}}>
                        
                        <CardSection style={{flex: 1}}>
                            <InputSansLabel 
                            active={true}
                            editable={true}
                            placeholder={this.state.inputPlaceholder}
                            value={this.state.inputText}
                            onChangeText={(inputText) => {this.setState({inputText})}}
                            onFocus={() => {}}
                            onEndEditing={() => this.toggleAlert()}
                            /> 
                        </CardSection>
                        <CardSection style={{flex: 5}}>
                            <TriangleColorPicker
                            onColorSelected={(color) => this.setState({color})}
                            style={{flex: 1}}
                            />
                        </CardSection>
                        <CardSection style={{flex: 1}} last>
                            <ButtonForm
                            onPress = {() => { this.onAcceptButtonPress() }} 
                            >{this.state.buttonTitle}</ButtonForm>
                        </CardSection>
                        
                    </Card>
                    {this.renderAlert()}
                    <View style={{flex: 1}}></View> 
                </View>
            </TouchableWithoutFeedback> 
        );
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,   
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    button: {
        flex: 1,  
        paddingRight: 0,
        marginRight: 0,
    },
    actionButtonIcon: {
        fontSize: 40,
        height: 40,
        color: APP_COLORS.primary,
      },

    showlist : {
        paddingHorizontal: 10,
        paddingTop: 10,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        borderColor: APP_COLORS.lines || 'gray',
        position: 'relative',
    },
    alertContainer: {
        position: 'absolute',
        left: '12%',
        right: '12%',
        bottom: '30%',
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 250, 
        paddingHorizontal: 10,    
        paddingVertical: 5,
        borderRadius: 5,
    }, 
    
});

export default AddCategoryComponent;




