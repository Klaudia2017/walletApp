import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet, AsyncStorage } from 'react-native';
import { Pie } from 'react-native-pathjs-charts'
import {APP_COLORS} from '../../style/color';
import {ErrorModal, Card, CardSection} from '../common'
import Icon from 'react-native-vector-icons/EvilIcons';

class ResumeAnnualComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: '',
      errorVisible: false,

      listOfDepenses: null,
      categoryList: [],
      budgetLeft: 0,
      data: null,
    }
  }

  static navigationOptions =({navigation}) => {
    return {
        title: 'RÉSUMÉ',
        headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
        headerStyle: { 
            backgroundColor: APP_COLORS.darkPrimary,  
            paddingHorizontal: 15,
        },
        headerTitleStyle: {
          color : APP_COLORS.primary,  
          alignSelf: 'center', 
          paddingRight: 50, 
          fontWeight: 'normal',
          fontSize: 15,
        }
    }  
  }  

  componentDidMount() {
    this.getData().then( () => { this.convertToPieChartData() } ).done();
  }

  getData = async () => {
    try {
        let listOfDepenses = await AsyncStorage.getItem('listOfDepenses');
        let categoryList = await AsyncStorage.getItem('categoryList');
        let BugdetValue = await AsyncStorage.getItem('BugdetValue');
        let totalOfExpenses = await AsyncStorage.getItem('totalOfExpenses'); 
        if (listOfDepenses && categoryList){
          listOfDepenses = JSON.parse(listOfDepenses);
          categoryList = JSON.parse(categoryList);
          BugdetValue = JSON.parse(BugdetValue);
          totalOfExpenses = JSON.parse(totalOfExpenses);
          let budgetLeft = Number(BugdetValue) - Number(totalOfExpenses);
          this.setState({listOfDepenses, categoryList, budgetLeft});   
        } else {  
            let error = "Liste des dépenses vidé" ;
            this.toggleError(error);
        }
    } catch (error) {   
        let error = "Erreur au niveau d'recuperation de données";
        this.toggleError(error);
    }   
  }

  convertToPieChartData = () => {
    var data = [];
    let {listOfDepenses, categoryList} = this.state;

    let hexColorbudget = '#cccccc';
    let colorbudget = this.convertColor(hexColorbudget);
    data.push({ 'name' : 'budget' , 'value' : this.state.budgetLeft, color: colorbudget, hexColor: hexColorbudget}) 

    categoryList.forEach((el) => {
      let categoryName = el.name;
      let depensesForCategory = 0;
      let hexColor = el.color;
      let color = this.convertColor(el.color);

      listOfDepenses.forEach(function(element){
        if(element.category.name == categoryName){
          depensesForCategory += parseInt(element.value);
        }
      });
     data.push({ 'name' : categoryName , 'value' : depensesForCategory, color: color , hexColor: hexColor });
    });
   
    console.log('data', data);
    this.setState({data});
  }

  // Pour convertir hex to RGB 
  convertColor = (hex) => {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
      'r': parseInt(result[1], 16),
      'g': parseInt(result[2], 16),
      'b': parseInt(result[3], 16)
    } : null;
  }
  renderBadge = (color) => { 
    return ( 
        <View style={[styles.circle, {backgroundColor: color}]} >
        </View>
    );
  }
  
  renderList = () => {
    return this.state.data.map( (el, i) => {
      return (  
        <View key={i} style={styles.descriptionItem}>
          {this.renderBadge(el.hexColor)} 
          <Text style={styles.descriptionText} >{el.name.toUpperCase()} -  {el.value} €</Text>
        </View>
      );  
    });
  }

  toggleError = ({error}) => {
    this.setState({ error, errorVisible: !this.state.errorVisible });
    setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
  }
  render() {
    let {data} = this.state;
    let options = {
      margin: {
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      },
      width: 350,
      height: 350,
      color: '#2980B9',  
      r: 60,
      R: 140,
      legendPosition: 'center',
      animate: {
        type: 'oneByOne',
        duration: 200,
        fillTransition: 3
      },
      label: {
        fontFamily: 'Arial',
        fontSize: 18,
        fontWeight: true,
        color: 'transparent'
      }
    }

    return (
      <ScrollView >
       
        { data && 
          <Card>
            <CardSection style={{justifyContent: 'center'}}>
              <Pie 
              data={data}
              options={options}   
              accessorKey="value"  
              />
            </CardSection>   
            <CardSection last>
              <View style={styles.description} >
                {this.renderList()}
              </View>
            </CardSection>
          </Card> 
           }
      </ScrollView>   
    )
  }
}

export default ResumeAnnualComponent;

const styles = StyleSheet.create({
  circle: {
    borderRadius: 8,
    width: 16,
    height: 16,  
  },
  description: {
    display: 'flex',
    flexDirection: 'column',
    alignSelf: 'flex-start',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingLeft: 20,
  },
  descriptionItem: {
    display: 'flex',  
    flexDirection: 'row',
    paddingTop: 8,
  },
  descriptionText: {
    fontSize: 14,
    paddingLeft: 10,

  }
});
