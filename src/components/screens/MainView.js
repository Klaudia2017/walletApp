
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, StatusBar, AsyncStorage} from 'react-native';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/Ionicons';
import { TabNavigator, StackNavigator, DrawerNavigator} from 'react-navigation';

import NewExpenseComponent from './NewExpenseComponent';
import EditOldExpenseComponent from './EditOldExpenseComponent';
import BudgetEditComponent from './BudgetEditComponent';
import EditProfilComponent from './EditProfilComponent';
import ResumeAnnualComponent from './ResumeAnnualComponent';
import ResumeInProgressComponent from './ResumeInProgressComponent';
import ResumeMonthlyComponent from './ResumeMonthlyComponent';
import VisualisationComponent from './VisualisationComponent';
import AddBudgetComponent from './AddBudgetComponent';
import ListOfDepenses from './ListOfDepenses';
import AddCategoryComponent from './AddCategoryComponent';
import CategoryList from './CategoryList';
import SettingsComponent from './SettingsComponent';

const StackNavigatorAddBudget= StackNavigator({
    AddBudgetView: {
      screen: AddBudgetComponent,
    }}),

    StackNavigatorNewExpense= StackNavigator({
    NewExpenseView: {
      screen: NewExpenseComponent,
    }}),

    StackNavigatorAddCategory= StackNavigator({
        AddCategoryView: {
          screen: AddCategoryComponent,
    }}),

    StackNavigatorListOfDepenses= StackNavigator({
    ListOfDepensesView: {
          screen: ListOfDepenses,
    }}),

    StackNavigatorBudgetEdit = StackNavigator({
    BudgetEditView: {
        screen: BudgetEditComponent,
    }}),

    StackNavigatorEditOldExpense= StackNavigator({
    EditOldExpenseView: {
        screen: EditOldExpenseComponent,
    }}),

    StackNavigatorEditProfil = StackNavigator({
    EditProfilView: {
        screen: EditProfilComponent,
    }}),

    StackNavigatorResumeAnnual = StackNavigator({
    ResumeAnnualView: {
      screen: ResumeAnnualComponent,
    }}),

    StackNavigatorResumeInProgress= StackNavigator({
    ResumeInProgressView: {
        screen: ResumeInProgressComponent,
    }}),

    StackNavigatorResumeMonthly = StackNavigator({
    ResumeMonthlyView: {
        screen: ResumeMonthlyComponent,
    }}),

    StackNavigatorVisualisation = StackNavigator({
    VisualisationView: {
        screen: VisualisationComponent,
    }});

    StackNavigatorCategoryList = StackNavigator({
        CategoryListView: {
            screen: CategoryList,
    }});
    
    StackNavigatorSettingsComponent = StackNavigator({
        SettingsComponentView: {
            screen: SettingsComponent,
    }});

    var MyDrawer = DrawerNavigator({
 
    ViewSettingsComponent: {
        screen: StackNavigatorSettingsComponent,
        title : 'PARAMETRES'
    },
    ViewAddCategory: {
        screen: StackNavigatorAddCategory,
        title : 'UNE NOUVELLE CATÉGORIE'
    },
    ViewCategoryList: {
        screen: StackNavigatorCategoryList,
        title : 'LIST DES CATÉGORIES'
    },
    ViewListOfDepenses: {
        screen: StackNavigatorListOfDepenses,
        title : 'ListOfDepenses'
    },
    BudgetEditView: {
        screen: StackNavigatorBudgetEdit,
        title : 'EDITER LE BUGET'
    },
    ViewBudgetEdit: {
        screen: StackNavigatorVisualisation,
        title : 'VISUALISATION'
    },
    ViewNewExpense: {
        screen: StackNavigatorNewExpense,
        title : 'AJOUTEZ'
    }
  }); 




class MainView extends Component {

   

    static navigationOptions =({navigation}) => {
        return {
            title: 'MAIN',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF",paddingLeft: 15}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                color : APP_COLORS.primary,
               
        }
      }  
    }

    render() {
        
        return (
          <View >
                <MyDrawer/>
          </View>
        );
      }
}
    
 const styles = StyleSheet.create({
      container: {
        flex: 1
      },
 });

export default MainView;
