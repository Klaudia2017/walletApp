import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, AsyncStorage, FlatList, TouchableOpacity, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { StackNavigator } from 'react-navigation';
import moment from 'moment';
import 'moment/locale/fr';
import { Button, ListItem, ListItemSection, Card, CardSection, ButtonForm, FadingView, ErrorModal } from '../common';  // supr
import {ActionButton} from '../common';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
moment.locale('fr');
const SCREEN_WIDTH = Dimensions.get('window').width;
class ListOfDepenses extends Component {  
    constructor (props) {  
        super(props)
        this.state = {
            bottomBarTitleLeft: "votre budget",
            bottomBarTitleRight: "votre depenses",
            monnais: '€',
            
            BugdetValue: 0,
            budgetLeft: 0,
            totalOfExpenses: 0,

            error: '',
            errorVisible: false,

            data: [],
            selected: '',

            isModalVisible: false,
            selectedItemId: null,
            
        }
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'LISTE DES DÉPENSES',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,  
                paddingHorizontal: 15,
            },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                alignSelf: 'center', 
                paddingRight: 50, 
                fontWeight: 'normal',
                fontSize: 15,
            }
        }  
    }  

    componentDidMount() {
        this.getListOfDepenses().then( () => { this.getBudget() } ).done();
    }

    getBudget = async () => {
        try {
            let BugdetValue = await AsyncStorage.getItem('BugdetValue');
            if (BugdetValue !== null){
                this.setState({ BugdetValue: JSON.parse(BugdetValue) });  
                this.calculeBudget()
            } 
        } catch (error) {
            error: "Erreur au niveau d'recuperation de budget";
            this.toggleError({error});
        }
    }
    getListOfDepenses = async () => {
        try {
            let listOfDepenses = await AsyncStorage.getItem('listOfDepenses');
            let totalOfExpenses = await AsyncStorage.getItem('totalOfExpenses');
            if (listOfDepenses && totalOfExpenses){
                listOfDepenses = JSON.parse(listOfDepenses);
                totalOfExpenses = JSON.parse(totalOfExpenses);
                this.setState({data: Array.from(listOfDepenses) });  
                this.setState({ totalOfExpenses: totalOfExpenses });
            } else { 
                error = 'Liste des dépenses vidé, s\'il vous plaît ajouter depense en cliquant sur le bouton en bas de l\'écran ...' ;
                this.toggleError({error}); 
            }
    
        } catch (error) {   
            error: "Erreur au niveau d'ajout de catégorie ";
            this.toggleError({error}); 
        }   
    }

    calculeBudget = () => {
        var {BugdetValue, totalOfExpenses} = this.state;
        let budgetLeft = Number(BugdetValue) - Number(totalOfExpenses); 
        this.setState({ budgetLeft });
    }
    
    removeDepense = async () => {
        let time = this.state.selectedItemId;
        let listOfDepenses = this.state.data;
        let { totalOfExpenses } = this.state;
        let OldExpense = {};
        
        listOfDepenses = listOfDepenses.map((el, index) => {
            if(el.time == time){
                OldExpense = el;
            }
            return el;
        },OldExpense);
        totalOfExpenses = Number(totalOfExpenses) - Number(OldExpense.value);
        listOfDepenses = listOfDepenses.filter(item => item.time !== time);
        this.setState({ data : Array.from(listOfDepenses), totalOfExpenses });
        try {
            await AsyncStorage.multiSet([ ['listOfDepenses', JSON.stringify(listOfDepenses)], ['totalOfExpenses', JSON.stringify(totalOfExpenses)] ]);
        } catch (error) {
            error : "Erreur au niveau des mis a jour de list des catégories ";
            this.toggleError({error}); 
        }
    }
    
    onRemoveDepenseClick = () => {
        this.removeDepense();
        this.toggleModal();
    }

    renderBadge = (item) => {
        let color = item.category.color; 
        return ( 
            <View style={[styles.circle, {backgroundColor: color}]} >
            </View>
        );
    }

    onPressAddButton = () => {
        var {navigate} = this.props.navigation;
        navigate('NewExpenseView');
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible })
    }
 
    goToModifyDepense = () => {
        var {navigate} = this.props.navigation;
        navigate('NewExpenseView', {'depenseID': this.state.selectedItemId});
    }

    onPressItem = (id) => {
        this.toggleModal();
        this.setState({ selectedItemId:  id });
    };
render() {
    
    const { container, itemTime, itemText, itemIcone, itemDescription, itemCategory, itemValeur, 
        actionButtonIcon, bottomBar, barDescription, barValue, modalText } = styles;

    return (
            <View style={container}>
                
                <View style={{flex: 10}}>
                    <ErrorModal visible={this.state.errorVisible}>{this.state.error}</ErrorModal>    
                    <FlatList
                        data={this.state.data}
                        extraData={this.state}
                        keyExtractor={item => item.time}
                        renderItem={({item}) => 
                            <ListItem   
                            id={item.time}
                            onPressItem={this.onPressItem}
                            >
                                <View style={itemIcone}>
                                    {this.renderBadge(item)}
                                </View>  
                                <View style={itemText}>
                                    <View >
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={itemCategory}>{item.category.name}</Text>
                                            <Text style={itemDescription}>{item.description}</Text>
                                        </View>
                                        <Text style={itemTime}>{moment(item.time).format('LL')}</Text>
                                    </View>
                                    <Text style={itemValeur}>{item.value} {this.state.monnais}</Text>  
                                </View>
                            </ListItem>
                        }
                    />       
                </View>
                
                <View style={bottomBar}>  
                    <View>
                        <Text style={barDescription}>{this.state.bottomBarTitleLeft}</Text>
                        <Text style={barValue}>{this.state.budgetLeft} {this.state.monnais}</Text>
                    </View>
                   
                    <View style={{alignItems: 'flex-end'}}>
                        <Text style={barDescription}>{this.state.bottomBarTitleRight}</Text>
                        <Text style={barValue}>{this.state.totalOfExpenses} {this.state.monnais}</Text>
                    </View>
                </View>
                <View style={styles.actionButtonContainer2}>
                    <ActionButton onPress={this.onPressAddButton} >
                        <IconIonicons name="ios-add" size={45}  color={APP_COLORS.primary} />
                    </ActionButton>
                </View>
                <Modal isVisible={this.state.isModalVisible}>
                    <Card>
                        <TouchableOpacity onPress={this.toggleModal} style={{ alignItems: 'flex-end', paddingTop: 10, paddingRight: 10 }} >
                            <Icon name="close" size={25} style={{color: APP_COLORS.secondaryText}} />
                        </TouchableOpacity>
                        <CardSection>
                            <Text style={modalText}>Que voudriez-vous faire ...</Text>
                        </CardSection>
                        <CardSection last={true}>
                            <ButtonForm onPress={ this.goToModifyDepense }>modifier</ButtonForm>
                            <View style={{width: 10}}></View>
                            <ButtonForm onPress={this.onRemoveDepenseClick}>supprimer</ButtonForm>
                        </CardSection>  
                    </Card>
                </Modal>
               
            </View>     
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    itemIcone: {
       flex: 1, 
       justifyContent: 'center',
    },
    circle: {
        borderRadius: 8,
        width: 16,
        height: 16,  
    },
    itemText: {
       flex: 10,
       flexDirection: 'row',
       alignItems: 'center', 
       justifyContent: 'space-between'
    },
    itemDescription: {
        paddingLeft: 6,
        lineHeight: 20,
        fontSize: 15,
        color: APP_COLORS.secondaryText,
    },
    itemTime:  {
        paddingLeft: 2,
        lineHeight: 20,
        fontSize: 15,
        color: APP_COLORS.secondaryText,
    },
    itemCategory: {
        lineHeight: 20,
        fontSize: 18,
        color: APP_COLORS.primaryAction,
        fontWeight: '600', 
    },
    itemValeur: {
        fontSize: 18,
        color: APP_COLORS.primaryAction,
        fontWeight: '600',
    },
    bottomBar: {
        flex: 1, 
        backgroundColor: APP_COLORS.darkPrimary,
        alignItems: 'center', 
        justifyContent: 'space-between',
        flexDirection: 'row',
        paddingHorizontal: 20,
        height: 60,
    },
    actionButtonContainer: {
        position: 'absolute',
        left: '50%', 
        right: 0,
        bottom: 30,
        marginLeft: 0, 
        marginRight: 0,
        width: SCREEN_WIDTH,
    },
    actionButtonContainer2: {
        position: 'absolute',
        bottom: 15,    
        alignSelf: 'center',
    },
    actionButtonIcon: {
        height: 35,  
        color: APP_COLORS.primary,
    },
    barDescription: {
        fontSize: 16,
        color: APP_COLORS.primary,
        fontWeight: '400',
        lineHeight: 24,
    },
    barValue: {
        fontSize: 18,
        color: APP_COLORS.primary,
        fontWeight: '600',
    },
    modalText: {
        fontSize: 18,
        paddingHorizontal: 10,
        paddingVertical: 20,
        color: APP_COLORS.secondaryText,
    }
});

export default ListOfDepenses;



