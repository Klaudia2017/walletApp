import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, Keyboard, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color';
import Icon from 'react-native-vector-icons/EvilIcons';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import { InputForm, ButtonForm, FadingView, ErrorModal, ActionButton, Card, CardSection} from '../common';

class AddBudgetComponent extends Component {

    constructor (props) {
        super(props)
        this.state = {
            inputLabel: "Votre budget",
            inputText: '',
            inputPlaceholder: '0',

            error : '',
            errorVisible: false,
        }
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'ÉTABLIR LE BUDGET',
            headerLeft: (<Icon name="navicon" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerStyle: { 
                backgroundColor: APP_COLORS.darkPrimary,
                paddingHorizontal: 15,   
             },
            headerTitleStyle: {
                color : APP_COLORS.primary,  
                alignSelf: 'center', 
                paddingRight: 50, 
                fontWeight: 'normal',
                fontSize: 15,
            }
      }  
    }  

    componentDidMount (){
        let {params} = this.props.navigation.state;
        if(params){
            if(params.budget) this.setState({'inputText': params.budget});
        }         
    }

    onPressButton = () => {
        this.setBudget().done();
    }
    setBudget = async () => {
        let budget = this.state.inputText;
        if( parseInt(budget) > 0 ){
            try {
                await AsyncStorage.setItem('BugdetValue', JSON.stringify(budget));
                this.props.navigation.navigate('ListOfDepensesScreen') ;
            } catch (error) {
                error = 'Erreur au niveau d\'ajout de budget' ;
                this.toggleError({error});
            }
        } else{
            error = 'Veuillez entrer toutes les informations demandées ' ;
            this.toggleError({error});
        }
    }

    toggleError = ({error}) => {
        this.setState({ error, errorVisible: !this.state.errorVisible });
        setTimeout(() => {this.setState({errorVisible: !this.state.errorVisible })}, 3000);
    }
        
    renderError = () => {  
        const { errorContainer, errorText } = styles;
        const { errorVisible } = this.state;
        
        return ( <FadingView visible={errorVisible} style={errorContainer}>  
                    <Text style={errorText} >
                        {this.state.error}    
                    </Text>
                </FadingView>  
        );     
    }

    // supprimer tous les caractères non numériques du texte d'entrée
    onChangedNumber = ({inputText}) => {
        this.setState({
            inputText: inputText.replace(/[^0-9]/g, ''),
        });
    }
    render() {
        return (
            <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false} >
            <View style={styles.container}>
                <Card>
                    <CardSection> 
                        <InputForm 
                        active={true}
                        editable={true}  
                        label={this.state.inputLabel}
                        value={this.state.inputText}
                        onChangeText={(inputText) =>  this.onChangedNumber({inputText})}
                        placeholder={this.state.inputPlaceholder}
                        symboleMonnaie='€'
                        keyboardType={'numeric'}
                        onFocus={() => {}}
                        onBlur={() => {}}
                        /> 
                    </CardSection>
                    <CardSection  style={{padding: 20, alignSelf: 'flex-end'}}>    
                        <ActionButton onPress={this.onPressButton} >  
                            <IconIonicons name="ios-checkmark" size={45}  style={styles.buttonIcon} />
                        </ActionButton>
                    </CardSection>
                </Card>
                <ErrorModal visible={this.state.errorVisible}>{this.state.error}</ErrorModal>
            </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: APP_COLORS.lightPrimaryColor,
    },
    section1: {
        flex: 3,
    },
    section2: {
        flex: 1,
    },
    section3: {
        flex: 3,
        alignItems: 'flex-end',
        paddingRight: 20,
    },
    input: {
        paddingHorizontal: 20,
    },
    errorContainer: {
        backgroundColor: APP_COLORS.darkPrimary || 'gray',
        width: 300, 
        marginTop: 40,
        alignSelf: 'center',  
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5,
    },
    errorText: {
        fontSize: 14,
        color: APP_COLORS.primary || 'white',
        lineHeight: 22,
    },  
    circleButton: {
        height: 60,
        width: 60,
        borderRadius: 30,   
        backgroundColor: APP_COLORS.accent || 'gray',  
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor:  '#000',
        shadowOffset: { width: 0, height: 5 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
    },
    buttonIcon: {
        color: APP_COLORS.primary,
    },
    
});


export default AddBudgetComponent;
